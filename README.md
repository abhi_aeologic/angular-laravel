# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a boilerplate for Angular 8 and Laravel 6.2 
* This Boilerplate has JWT integration, Login page with authentication, and a dashboard page.

### How do I get set up? ###

1. Git clone this repository and goto the folder.
2. Use cd api to go inside API folder and run composer install.
3. Import Database fom root folder database.sql.
3. Setup .env file.
4. Run php artisan serve to serve up the API.
5. Got to frontend folder using cd ../frontend and run npm install
6. setup environment.js file with your configuration.
7. And finally run ng serve to serve up the frontend.

### Who do I talk to? ###

* Abhishek Gupta
* abhishekgupta00143@gmail.com