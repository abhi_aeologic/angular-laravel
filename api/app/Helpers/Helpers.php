<?php

use App\User;
use Illuminate\Support\Facades\File;

function sendResponse($status = 503, $message = 'Something went wrong. Please try again.', $data = []){
        $response = new \stdClass();
        $response->status = $status;
        $response->message = $message;
        $response->result = $data;
        return response()->json($response, 200);
    }

    //Base64 Image Storage Function
    function uploadImage($base64Image, $path){
        $image = str_replace('data:image/png;base64,', '', $base64Image);
        $image = str_replace(' ', '+', $image);

        $imageName = Str::random(32).'_'.date('dmy_His').'.png';
        $completePath = $path.'/'.$imageName;

        Storage::put($completePath, base64_decode($image));
        return $completePath;
    }

    function sendSMS($mobileNumber, $message){

        try{
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,            "https://platform.clickatell.com/messages" );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt($ch, CURLOPT_POST,           1 );
            curl_setopt($ch, CURLOPT_POSTFIELDS,     '
            {
                "content": "'.$message.'",
                "to": ["'.$mobileNumber.'"],
                "clientMessageId": "878839"
            }
            ' );
            curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json', 'Authorization:b2-C6pxASveC15L0eaLm6Q=='));

            return curl_exec ($ch);
        } catch (\Exception $e){
          dd($e->getMessage());
        }
    }

    //Admin upload Image Function

    function saveImage($dataSet) {
        $file      = $dataSet->file('image');
        $filename  = $file->getClientOriginalName();
//        $extension = $file->getClientOriginalExtension();
//        $picture   = date('His').'-'.$filename;
        $imageName = $filename;
//        $imageName = $filename.'_'.date('dmy_His'). $extension;
        $completePath = 'admin'.'/'.$imageName;

        $file->move(storage_path('app/public/admin/', $imageName));

        return $completePath;
    }
