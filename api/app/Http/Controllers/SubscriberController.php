<?php

namespace App\Http\Controllers;

use App\EmergencyContact;
use App\Http\Requests\AdditionalInfo;
use App\Http\Requests\AuraTokenSaveRequest;
use App\Http\Requests\EditSubscriberRequest;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\SignInRequest;
use App\Http\Requests\SubscriberRegistrationFormRequest;
use App\Http\Requests\SubscriberSetting;
use App\SubscriberRegistrationModeHistory;
use App\User;
use App\UserSetting;
use App\UserVehicle;
use App\Voucher;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class SubscriberController extends Controller
{

    protected function signIn(SignInRequest $request){
        try{
            $credentials['email_id'] = $request->user_id;
            $credentials['password'] = $request->password;
            $credentials['role_id'] = 1;

            $token = null;

            if (!$token = JWTAuth::attempt($credentials)) {
                return sendResponse(401, 'Invalid User Id or Password');
            }

            $currentUser = JWTAuth::user();
            $returnData = ['token'=>$token, 'user_profile'=>$currentUser];

            return sendResponse(200, 'Subscriber Signed In Successfully', $returnData);

        }catch (\Exception $e){
            return sendResponse(504, $e->getMessage());
        }
    }

}
