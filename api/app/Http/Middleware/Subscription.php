<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class Subscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userModel = new User();

        if($userModel->subsciptionValidate())
            return $next($request);

        return sendResponse(401, 'Subscription Expire!');
    }
}
