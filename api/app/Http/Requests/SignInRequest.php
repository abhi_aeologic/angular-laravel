<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class SignInRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'password' => 'required'
        ];
    }

    protected function failedValidation(Validator $validator) {

        $errorMessage = str_replace('.', ' ', $validator->messages()->first());
        $response = new \stdClass();
        $response->status = 401;
        $response->message = $errorMessage;
        $response->result = [];
        throw new HttpResponseException(response()->json($response, 401));
    }
}
