<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email_id', 'password', 'surname', 'gender', 'role_id', 'phone_number', 'dob', 'profile_pic', 'device_id', 'aura_access_token', 'expiry_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function changePassword($data){

        return $this->where('phone_number',$data['phone_number'])
                    ->update(['password'=>bcrypt($data['password'])]);
    }

    public function saveAuraAccessToken($data){
        return $this->where('id', $data['user_id'])
            ->update(['aura_access_token'=> $data['access_token'] ]);
    }

    public function subsciptionValidate(){

        if(auth('api')->user()){
            return $this->where('id', auth('api')->user()->id)
                        ->where('expiry_date','>=', Carbon::now())
                        ->first();
        }else{
            return false;
        }

    }

    public function saveUser($data){
        try{
            $this->surname = ( isset($data['personal_details']['surname']) && $data['personal_details']['surname'] ) ? $data['personal_details']['surname'] : '';
            $this->name = ( isset($data['personal_details']['name']) && $data['personal_details']['name'] ) ? $data['personal_details']['name'] : '';
            $this->password  = ( isset($data['security_details']['password']) && $data['security_details']['password'] ) ? bcrypt($data['security_details']['password']) : '';
            $this->role_id  = 2;
            $this->email_id  = ( isset($data['personal_details']['email']) && $data['personal_details']['email'] ) ? $data['personal_details']['email'] : '';
            $this->phone_number  = ( isset($data['personal_details']['phone_number']) && $data['personal_details']['phone_number'] ) ? $data['personal_details']['phone_number'] : '';
            $this->dob  = ( isset($data['security_details']['date_of_birth']) && $data['security_details']['date_of_birth'] ) ? $data['security_details']['date_of_birth'] : '';

            $this->expiry_date = ( isset($data['period']) && $data['period'] ) ? Carbon::now()->addDay($data['period']) : '';
            $this->device_id = ( isset($data['device_id']) && $data['device_id'] ) ? $data['device_id'] : '';
//            $this->payment_type  = ( isset($data['subscription_details']['payment_type']) && $data['subscription_details']['payment_type'] ) ? $data['subscription_details']['payment_type'] : '';
//            $this->voucher_number  = ( isset($data['subscription_details']['voucher_detail']['voucher_number']) && $data['subscription_details']['voucher_detail']['voucher_number'] ) ? $data['subscription_details']['voucher_detail']['voucher_number'] : '';
            $this->save();
            return $this->id;
        }catch(\Exception $e){
            return false;
        }

    }

    public function updateSubscriberDetail($data){

        return $this->where('id', $data['id'])
                    ->update(['name' => $data['name'],
                              'dob' => $data['date_of_birth'],
                              'gender' => $data['gender'],
                              'profile_pic' => uploadImage($data['profile_photo'], 'subscriber'),
                              'phone_number'=> $data['cell_number']
                            ]);

    }

    public function getProfileDetail($data){

        return $this->find($data['user_id']);

    }

    public function updateSubscriber($data){
        $updatedInfo = null;
        if(( isset($data['gender']) && $data['gender'] )){
            $updatedInfo['gender'] = $data['gender'];
        }
        if(( isset($data['profile_photo']) && $data['profile_photo'] )){
            $updatedInfo['profile_pic'] = uploadImage($data['profile_photo'], 'subscriber');
        }

        if($updatedInfo){
            return $this->where('id',$data['user_id'])->update($updatedInfo);
        }

        return false;
    }

    public function getSubscriberList($data){

        $query = $this->with(['userSettings', 'userVehicles', 'userEmergencyContacts'])->where('role_id', 2);
        $response = new \stdClass();

        if(isset($data['search']['email_id']) && $data['search']['email_id']){
            $query->where('email_id',$data['search']['email_id']);
        }

        if(isset($data['id']) && $data['id']){
            $query->where('id',$data['id']);
            return $query->get()->toArray();
        }

        $query->orderBy('created_at', 'desc');
        if (isset($data['page'])) {
            $recordPerPage = 10;
            $page = $data['page'];

            $limit = $recordPerPage * ($page - 1);

            $response->result = $query
                ->skip($limit)->take($recordPerPage)->get();

            $response->paginate = $query
                ->paginate($recordPerPage);

        } else {
            $response->result = $query->get();
        }

        return $response;

    }

    public function getUserWithId($userId){
        return User::find($userId);
    }
    public function roles(){
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function userSettings(){
        return $this->hasOne(UserSetting::class, 'user_id');
    }

    public function userVehicles(){
        return $this->hasOne(UserVehicle::class, 'user_id');
    }

    public function userEmergencyContacts(){
        return $this->hasMany(EmergencyContact::class, 'user_id');
    }

    public function getProfilePicAttribute($value){
       if($value){

            return url('/storage/'.$value);
       }else{
            return null;
       }
    }

    public function editProfileDetail($data){

        return $this->find($data['user_id']);
    }

    public function updateUser($data = array(), $user_id = null){
        try{
           return $this->where('id',$user_id)->update($data);
        }catch(\Exception $e){
            return false;
        }

    }

}
