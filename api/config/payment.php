<?php

return [
    'entity' => env('PAYMENT_ENTITY', null),
    'token' => env('PAYMENT_TOKEN', null),
    'url' => env('CHECKOUT_URL', null),
    'paymentUrl' => env('PAYMENT_URL', null),
];
