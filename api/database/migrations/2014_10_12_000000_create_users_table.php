<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('surname');
            $table->string('name');
            $table->string('gender');
            $table->string('email_id')->unique();
            $table->string('password');
            $table->string('phone_number');
            $table->string('device_id');
            $table->string('payment_type')->nullable(false);
            $table->string('voucher_number')->nullable(false);
            $table->string('profile_pic')->nullable(false);
            $table->integer('role_id');
            $table->date('dob')->nullable(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
