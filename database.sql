/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.29-0ubuntu0.18.04.1 : Database - boilerplate_db
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`boilerplate_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `boilerplate_db`;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(100) DEFAULT NULL,
  `name` varchar(225) NOT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `password` varchar(225) NOT NULL,
  `role_id` int(11) NOT NULL,
  `email_id` varchar(225) NOT NULL,
  `phone_number` varchar(225) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `profile_pic` varchar(225) DEFAULT NULL,
  `device_id` varchar(225) DEFAULT NULL,
  `aura_access_token` text,
  `expiry_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`surname`,`name`,`gender`,`password`,`role_id`,`email_id`,`phone_number`,`dob`,`profile_pic`,`device_id`,`aura_access_token`,`expiry_date`,`created_at`,`updated_at`) values (1,'Mr.','Abhishek','male','$2y$10$OGq0sAxStJRXT5MGqk.9Iez0asQnGqrmLo3hmb9aO6.17GVzBzQJG',1,'abhishekg@aeologic.com','12345678','2019-12-17','','34ewrf3e','ggwefgehqfgehtwrgfwgfw',NULL,'2019-12-17 19:04:39','2020-02-04 09:08:49');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;