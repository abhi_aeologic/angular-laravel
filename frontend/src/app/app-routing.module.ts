import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {UsersComponent} from './users/users.component';


const routes: Routes = [
  { path: '', redirectTo: 'subscriber', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  { path: 'user', component: UsersComponent },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
