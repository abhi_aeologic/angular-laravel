import { Component, OnInit, TemplateRef } from '@angular/core';
import { CommonService } from '../../_services/common.service';
// import {HttpErrorResponse} from '@angular/common/http';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthenticationService } from '../../_services'
import { environment } from '../../../environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    authUser;
    baseUrl = environment.baseUrl;
    constructor(private router: Router,private spinner: NgxSpinnerService,
                private authenticationService: AuthenticationService,
                private formBuilder: FormBuilder,
                private _commonService: CommonService) { }

    ngOnInit() {
        const currentUser = JSON.parse(localStorage.getItem('boilerPlateUser'));
        this.authUser = currentUser['user_profile'];

    }

  logout() {
    this.authenticationService.logout();
  }

}
