import { Component, OnInit } from '@angular/core';
import { CommonService , AuthenticationService } from '../_services';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {

  roleId = null;
  currentUser = null;

  constructor(private _http: HttpClient, private router: Router, private authenticationService: AuthenticationService,
              private _commonService: CommonService,private spinner: NgxSpinnerService) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  ngOnInit() {
    const currentUser = JSON.parse(localStorage.getItem('boilerPlateUser'));

    if (currentUser) {
      this.roleId = currentUser.user_profile.role_id;
    } else {
      this.router.navigate(['/login']);
    }
  }

}
