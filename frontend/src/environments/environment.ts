export const environment = {
  production: false,
  apiUrl: 'http://localhost:8000/api',
  baseUrl: 'http://127.0.0.1:4200',
};
